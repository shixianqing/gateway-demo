package com.example;

import com.example.exception.BizException;
import com.netflix.hystrix.exception.HystrixTimeoutException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.ServerWebExchangeDecorator;

/**
 * @author:shixianqing
 * @Date:2021/12/9 13:38
 * @Description:
 **/
@RestController
@Slf4j
public class FallbackController {

    @RequestMapping("fallback")
    @ResponseStatus
    public String fallback(ServerWebExchange exchange) throws Exception {
        Exception exception = exchange.getAttribute(ServerWebExchangeUtils.HYSTRIX_EXECUTION_EXCEPTION_ATTR);
        ServerWebExchange delegate = ((ServerWebExchangeDecorator) exchange).getDelegate();
        log.error("接口调用失败，URL={}", delegate.getRequest().getURI(), exception);
        if (exception instanceof HystrixTimeoutException) {
            log.error("msg {}", "接口调用超时");
            throw new BizException("100","接口调用超时");
        } else if (exception != null && exception.getMessage() != null) {
            log.error("msg {}", "接口调用失败: " + exception.getMessage());
            throw new BizException("100","接口调用失败,失败原因："+exception.getMessage());
        } else {
            log.error("msg {}", "接口调用失败");
            throw new BizException("100","接口调用失败："+exception.getMessage());
        }
    }
}
