package com.example;

import com.alibaba.fastjson.JSONObject;
import com.example.exception.BizException;
import com.example.response.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.cloud.gateway.support.TimeoutException;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebExceptionHandler;
import reactor.core.publisher.Mono;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.LinkedHashSet;

/**
 * @author: shixianqing
 * @description:
 * @date: 2021-12-12 12:15
 */
@Component
@Order(-1)
@Slf4j
public class GlobalExceptionHandler implements WebExceptionHandler {

    @Override
    public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {

        //获取原始请求地址
        LinkedHashSet<URI> uris = exchange
                .getAttribute(ServerWebExchangeUtils.GATEWAY_ORIGINAL_REQUEST_URL_ATTR);
        String url;
        if (CollectionUtils.isEmpty(uris)) {
            url = exchange.getRequest().getURI().toString();
        } else {
            url = uris.stream().findFirst().get().toString();
        }
        log.error("url：{}，调用失败：",url,ex);
        R r = null;
        if (ex instanceof TimeoutException) {
            r = R.error("接口调用超时，请稍后重试");
        } else if (ex instanceof BizException) {
            BizException e = (BizException) ex;
            r = R.error(e.getMessage());
        } else if (ex instanceof ResponseStatusException) {
            ResponseStatusException e = (ResponseStatusException) ex;
            if (e.getStatus().is4xxClientError()) {
                r = R.error("服务不存在，请联系管理员");
            } else if (e.getStatus().is5xxServerError()) {
                r = R.error("系统异常，请联系管理员");
            }
        }else {
            r = R.error("接口调用失败，请稍后重试");
        }
        String responseStr = JSONObject.toJSONString(r);
        ServerHttpResponse response = exchange.getResponse();
        //处理响应乱码
        response.getHeaders().add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        return response.writeWith(Mono.fromSupplier(() -> {
            DataBufferFactory factory = response.bufferFactory();
            try {
                return factory.wrap(responseStr.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return factory.wrap(new byte[0]);
            }
        }));
    }
}
