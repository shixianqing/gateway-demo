package com.example.exception;

import lombok.Data;

/**
 * @author: shixianqing
 * @description:
 * @date: 2021-12-12 17:23
 */
@Data
public class BizException extends RuntimeException{

    private String code;

    private String message;

    public BizException(String code,String message) {
        super(message);
        this.code = code;
        this.message = message;
    }
}
