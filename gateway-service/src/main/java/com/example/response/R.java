package com.example.response;

/**
 * @author: shixianqing
 * @description:
 * @date: 2021-12-12 12:07
 */
public class R<T> {

    private String code;

    private String message;

    private T result;

    public static final String SUCCESS_CODE = "0";
    public static final String ERROR_CODE = "100";

    private R(T result) {
        this.result = result;
    }

    private R(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private R(String code, T result) {
        this.code = code;
        this.result = result;
    }

    public static R success(Object data) {
        return new R(SUCCESS_CODE,data);
    }

    public static R error(String message) {
        return new R(ERROR_CODE,message);
    }

    public static R error(String code, String message) {
        return new R(code,message);
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public T getResult() {
        return result;
    }
}
