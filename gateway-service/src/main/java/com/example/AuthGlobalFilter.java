package com.example;

import com.example.exception.BizException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @author: shixianqing
 * @description:
 * @date: 2021-12-12 11:56
 */
@Slf4j
@Component
public class AuthGlobalFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //鉴权
        List<String> name = exchange.getRequest().getHeaders().get("name");
        log.info("name：{}",name);
        String path = exchange.getRequest().getURI().getPath();
        //path：/a/helloword
        log.info("path：{}",path);
        String url = exchange.getRequest().getURI().toString();
        //url：http://localhost:8082/a/helloword
        log.info("url：{}",url);
        if (CollectionUtils.isEmpty(name)) {
            return Mono.error(new BizException("100","当前未登录"));
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
