package com.example;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author:shixianqing
 * @Date:2021/12/9 11:11
 * @Description: 限制请求的标识，可以根据用户名，客户端ip，进行限流
 **/
@Component
@Slf4j
public class MyKeyResolver implements KeyResolver {

    @Override
    public Mono<String> resolve(ServerWebExchange exchange) {
        String hostAddress = exchange.getRequest().getRemoteAddress().getAddress().getHostAddress();
        log.info("hostAddress：{}",hostAddress);
        return Mono.just(hostAddress);
    }
}
