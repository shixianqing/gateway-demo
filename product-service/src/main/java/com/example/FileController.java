package com.example;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;

/**
 * @author: shixianqing
 * @description:
 * @date: 2021-12-09 16:04
 */
@RestController
@RequestMapping("/file")
@Slf4j
public class FileController {

    @PostMapping("/upload")
    public String upload(@RequestParam("file")MultipartFile file,@RequestParam("userName") String userName) {

        log.info("文件名称：{}，userName：{}",file.getOriginalFilename(),userName);
        return file.getOriginalFilename();
    }

    @GetMapping("/download")
    public void download(HttpServletResponse response) throws UnsupportedEncodingException {
        String fileName = "C:\\Users\\14345\\Desktop\\个人简历.xlsx";
        FileInputStream fis = null;
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        response.setHeader("Content-disposition", "attachment;filename="+ URLEncoder.encode("个人简历.xlsx","UTF-8"));
        try {
            ServletOutputStream os = response.getOutputStream();
            fis = new FileInputStream(fileName);
            byte[] bytes = new byte[1024];
            int len;
            while ((len = fis.read(bytes)) != -1) {
                os.write(bytes,0,len);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
