package com.example;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author:shixianqing
 * @Date:2021/12/6 17:52
 * @Description:
 **/
@RestController
public class HelloController {

    @Value("${server.port}")
    private int port;

    @GetMapping("helloword")
    public String helloword(HttpServletRequest request) throws InterruptedException {
        Thread.sleep(5000);
        String foo = request.getParameter("foo");
        String header = request.getHeader("X-Request-Foo");
        String template = "hello word ---- 端口----%d";
        return String.format(template,port);
    }

    @PostMapping("post")
    public String post(HttpServletRequest request) {
        return "post==="+request.getParameter("foo");
    }

    @GetMapping("get/{segment}")
    public String get(@PathVariable("segment") String segment) {
        return "get---" + segment;
    }
}
