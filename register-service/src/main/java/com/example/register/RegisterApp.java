package com.example.register;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author:shixianqing
 * @Date:2021/12/6 15:59
 * @Description:
 **/
@SpringBootApplication
@EnableEurekaServer
public class RegisterApp {

    public static void main(String[] args) {
        SpringApplication.run(RegisterApp.class);
    }
}
