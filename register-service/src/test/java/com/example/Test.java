package com.example;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * @author:shixianqing
 * @Date:2021/12/6 16:17
 * @Description:
 **/
public class Test {


    public static void main(String[] args) {
        String str = "{\"tkBatchNo\": \"YSJ84012105191794080\", \"reportInfo\": \"{\\\"lineAdmNo\\\":\\\"2021051700003239\\\",\\\"rptorName\\\":\\\"u859bu4e16u78a7\\\",\\\"rptorIdType\\\":\\\"0\\\",\\\"rptorIdNo\\\":\\\"530181199407082625\\\",\\\"applyDate\\\":\\\"2021-05-17\\\",\\\"customerNo\\\":\\\"7700411417\\\",\\\"name\\\":\\\"u6731u798fu5168\\\",\\\"sex\\\":\\\"0\\\",\\\"idType\\\":\\\"0\\\",\\\"idNo\\\":\\\"320724199101164210\\\",\\\"brithDay\\\":\\\"1991-01-16\\\",\\\"accidentDate\\\":\\\"2021-05-17\\\",\\\"imageName\\\":[\\\"IM_2021051700003239_157391919_0001.jpg\\\",\\\"IM_2021051700003239_157391920_0002.jpg\\\",\\\"IM_2021051700003239_157391921_0003.jpg\\\",\\\"IM_2021051700003239_157391922_0004.jpg\\\",\\\"IM_2021051700003239_157391923_0005.jpg\\\",\\\"IM_2021051700003239_157391924_0006.jpg\\\",\\\"IM_2021051700003239_157391925_0007.jpg\\\",\\\"IM_2021051700003239_157391926_0008.jpg\\\",\\\"IM_2021051700003239_157391927_0009.jpg\\\",\\\"IM_2021051700003239_157391928_0010.jpg\\\",\\\"IM_2021051700003239_157391929_0011.jpg\\\"],\\\"grpContNo\\\":\\\"288401527783\\\",\\\"telephone\\\":\\\"18616662585\\\",\\\"applicantsBearing\\\":\\\"GX02\\\",\\\"bankList\\\":[{\\\"accountName\\\":\\\"u6731u798fu5168\\\",\\\"accountNo\\\":\\\"6214832145904285\\\",\\\"bank\\\":null,\\\"bankCode\\\":\\\"06\\\"}]}', \"tkPayFlag\": \"0\"}";


        JSONObject o = JSON.parseObject(str);

        System.out.println(o);

    }
}
